<?php

namespace Functional\Controller;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Uid\Uuid;

final class GetProductsControllerTest extends WebTestCase
{
    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testGetProducts()
    {
        $this->client->request(
            'GET',
            '/v1/products',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
        );

        $products = (array) json_decode($this->client->getResponse()->getContent())->body;

        $this->assertInstanceOf(JsonResponse::class, $this->client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(200, (string) $this->client->getResponse()->getStatusCode());
        $this->assertArrayHasKey('products', $products);
        $this->assertArrayHasKey('pagination', $products);

        static::tearDown();
    }
}
