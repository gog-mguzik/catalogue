<?php

namespace Functional\Controller;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Uid\Uuid;

final class GetProductControllerTest extends WebTestCase
{
    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testGetProduct()
    {
        $this->client->request(
            'POST',
            '/v1/products',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'name'      => Uuid::v4(),
                'price'     => rand(1, 999),
                'currency'  => 'USD'
            ])
        );

        $product = (array) json_decode($this->client->getResponse()->getContent())->body;

        $this->client->request(
            'GET',
            sprintf('/v1/products/%s', $product['id']),
        );

        $this->assertInstanceOf(JsonResponse::class, $this->client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(200, (string) $this->client->getResponse()->getStatusCode());

        static::tearDown();
    }

    public function testGetProductWithInvalidProductId()
    {
        $this->client->request(
            'GET',
            sprintf('/v1/products/%s', '59ef7590-2026-49d3-84fa-878469'),
        );

        $this->assertInstanceOf(JsonResponse::class, $this->client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(406, (string) $this->client->getResponse()->getStatusCode());

        static::tearDown();
    }

    public function testGetProductWithNonExistingProductId()
    {
        $this->client->request(
            'GET',
            sprintf('/v1/products/%s', Uuid::v4()),
        );

        $this->assertInstanceOf(JsonResponse::class, $this->client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(404, (string) $this->client->getResponse()->getStatusCode());

        static::tearDown();
    }
}
