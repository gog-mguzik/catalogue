<?php

namespace Functional\Controller;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Uid\Uuid;

final class AddProductControllerTest extends WebTestCase
{
    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testAddNewProduct()
    {
        $this->client->request(
            'POST',
            '/v1/products',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                    'name'      => Uuid::v4(),
                    'price'     => rand(1, 999),
                    'currency'  => 'USD'
            ])
        );

        $this->assertInstanceOf(JsonResponse::class, $this->client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(201, (string) $this->client->getResponse()->getStatusCode());

        static::tearDown();
    }

    public function testAddNewProductWithNonExistingCurrency()
    {
        $this->client->request(
            'POST',
            '/v1/products',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'name'      => Uuid::v4(),
                'price'     => rand(1, 999),
                'currency'  => 'GUZ'
            ])
        );

        $this->assertInstanceOf(JsonResponse::class, $this->client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(200, (string) $this->client->getResponse()->getStatusCode());

        static::tearDown();
    }

    public function testAddNewProductWithFloatPrice()
    {
        $this->client->request(
            'POST',
            '/v1/products',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'name'      => Uuid::v4(),
                'price'     => 4.54,
                'currency'  => 'PLN'
            ])
        );

        $this->assertInstanceOf(JsonResponse::class, $this->client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(201, (string) $this->client->getResponse()->getStatusCode());

        static::tearDown();
    }

    public function testAddNewProductWithStringPrice()
    {
        $this->client->request(
            'POST',
            '/v1/products',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'name'      => Uuid::v4(),
                'price'     => '7,23',
                'currency'  => 'USD'
            ])
        );

        $this->assertInstanceOf(JsonResponse::class, $this->client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(201, (string) $this->client->getResponse()->getStatusCode());

        static::tearDown();
    }

    public function testAddNewProductWithExistingProductName()
    {
        $this->client->request(
            'POST',
            '/v1/products',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'name'      => 'Fallout',
                'price'     => 643,
                'currency'  => 'EUR'
            ])
        );

        $this->assertInstanceOf(JsonResponse::class, $this->client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(406, (string) $this->client->getResponse()->getStatusCode());

        static::tearDown();
    }
}