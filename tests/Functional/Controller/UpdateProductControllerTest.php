<?php

namespace Functional\Controller;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Uid\Uuid;

final class UpdateProductControllerTest extends WebTestCase
{
    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = static::createClient();
    }

    public function testUpdateProductPatch()
    {
        $this->client->request(
            'POST',
            '/v1/products',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'name'      => Uuid::v4(),
                'price'     => 999,
                'currency'  => 'USD'
            ])
        );

        $product = (array) json_decode($this->client->getResponse()->getContent())->body;

        $this->client->request(
            'PATCH',
            sprintf('/v1/products/%s', $product['id']),
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'price' => 123,
            ])
        );

        $product = (array) json_decode($this->client->getResponse()->getContent())->body;

        $this->assertInstanceOf(JsonResponse::class, $this->client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(200, (string) $this->client->getResponse()->getStatusCode());
        $this->assertSame(123, $product['price']);

        static::tearDown();
    }

    public function testUpdateProductPut()
    {
        $this->client->request(
            'POST',
            '/v1/products',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'name'      => Uuid::v4(),
                'price'     => 999,
                'currency'  => 'USD'
            ])
        );

        $product = (array) json_decode($this->client->getResponse()->getContent())->body;

        $this->client->request(
            'PUT',
            sprintf('/v1/products/%s', $product['id']),
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'name'  => Uuid::v4(),
                'price' => 456,
                'currency' => 'EUR'
            ])
        );

        $product = (array) json_decode($this->client->getResponse()->getContent())->body;

        $this->assertInstanceOf(JsonResponse::class, $this->client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(200, (string) $this->client->getResponse()->getStatusCode());
        $this->assertSame(456, $product['price']);
        $this->assertSame('EUR', $product['currency']);

        static::tearDown();
    }

    public function testUpdateProductWithWrongCurrency()
    {
        $this->client->request(
            'POST',
            '/v1/products',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'name'      => Uuid::v4(),
                'price'     => 999,
                'currency'  => 'USD'
            ])
        );

        $product = (array) json_decode($this->client->getResponse()->getContent())->body;

        $this->client->request(
            'PATCH',
            sprintf('/v1/products/%s', $product['id']),
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'currency' => 'GUZ'
            ])
        );

        $this->assertInstanceOf(JsonResponse::class, $this->client->getResponse());
        $this->assertResponseHeaderSame('Content-Type', 'application/json');
        $this->assertResponseStatusCodeSame(406, (string) $this->client->getResponse()->getStatusCode());

        static::tearDown();
    }
}
