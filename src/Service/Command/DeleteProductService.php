<?php
declare(strict_types=1);

namespace App\Service\Command;

use App\Helper\PriceHelper;
use App\Helper\RequestHelper;
use App\Message\Command\DeleteProduct as DeleteProductCommand;
use App\Message\Query\Product as ProductQuery;
use App\Service\ServiceResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DeleteProductService extends ServiceResponse
{
    private const SUCCESSFUL_MESSAGE = 'Product deleted successfully.';

    public function __construct(
        private RequestHelper $requestHelper,
        private ValidatorInterface $validator,
        private MessageBusInterface $messageBus,
        private PriceHelper $priceHelper
    ) {}

    public function process(string $productId): JsonResponse
    {
        try {
            $this->messageBus
                ->dispatch(new ProductQuery(Uuid::fromString($productId)))
                ->last(HandledStamp::class);

            $this->messageBus
                ->dispatch(new DeleteProductCommand(Uuid::fromString($productId)))
                ->last(HandledStamp::class);

            $response = $this->parseResponse(
                self::SUCCESSFUL_MESSAGE,
                Response::HTTP_OK,
                []
            );
        } catch (InvalidArgumentException $exception) {
            $response = $this->parseResponse($exception->getMessage(), Response::HTTP_NOT_ACCEPTABLE);
        } catch (\Exception $exception) {
            $response = $this->parseResponse($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse($response, $response['code']);
    }
}
