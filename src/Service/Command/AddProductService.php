<?php
declare(strict_types=1);

namespace App\Service\Command;

use App\Entity\Product;
use App\Exception\WrongParamsException;
use App\Helper\PriceHelper;
use App\Helper\RequestHelper;
use App\Message\Command\AddProduct as AddProductCommand;
use App\Service\ServiceResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class AddProductService extends ServiceResponse
{
    private const SUCCESSFUL_MESSAGE = 'Product added successfully.';

    public function __construct(
        private RequestHelper $requestHelper,
        private ValidatorInterface $validator,
        private MessageBusInterface $messageBus,
        private PriceHelper $priceHelper
    ) {}

    public function process(Request $request): JsonResponse
    {
        try {
            $requestParams = $this->requestHelper->getRequestParams($request);

            $product = new Product(
                $requestParams['name'],
                $this->priceHelper->changePriceToInt($requestParams['price']),
                $requestParams['currency']
            );

            $errors = $this->validator->validate($product);

            if ($errors->count() > 0) {
                $messages = [];

                foreach ($errors as $error) {
                    $messages[] = sprintf("%s: %s", $error->getPropertyPath(), $error->getMessage());
                }

                throw new WrongParamsException((string) $errors, Response::HTTP_NOT_ACCEPTABLE);
            }

            $newProduct = $this->messageBus
                ->dispatch(new AddProductCommand($product->getTitle(), $product->getPrice(), $product->getCurrency()))
                ->last(HandledStamp::class)
                ->getResult();

            $response = $this->parseResponse(
                self::SUCCESSFUL_MESSAGE,
                Response::HTTP_CREATED,
                $newProduct->toArray()
            );
        } catch (InvalidArgumentException $exception) {
            $response = $this->parseResponse($exception->getMessage(), Response::HTTP_NOT_ACCEPTABLE);
        } catch (\Exception $exception) {
            $response = $this->parseResponse($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse($response, $response['code']);
    }
}
