<?php
declare(strict_types=1);

namespace App\Service\Command;

use App\Exception\WrongParamsException;
use App\Helper\PriceHelper;
use App\Helper\RequestHelper;
use App\Message\Command\EditProduct as EditProductCommand;
use App\Message\Query\Product as ProductQuery;
use App\Service\ServiceResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UpdateProductService extends ServiceResponse
{
    private const SUCCESSFUL_MESSAGE = 'Product updated successfully.';

    public function __construct(
        private RequestHelper $requestHelper,
        private ValidatorInterface $validator,
        private MessageBusInterface $messageBus,
        private PriceHelper $priceHelper
    ) {}

    public function process(string $productId, Request $request): JsonResponse
    {
        try {
            $requestParams = $this->requestHelper->getRequestParams($request);

            $handledStamp = $this->messageBus
                ->dispatch(new ProductQuery(Uuid::fromString($productId)))
                ->last(HandledStamp::class);

            $product = $handledStamp->getResult();

            if (isset($requestParams['title'])) $product->setName($requestParams['title']);
            if (isset($requestParams['price'])) $product->setPrice(
                $this->priceHelper->changePriceToInt($requestParams['price'])
            );
            if (isset($requestParams['currency'])) $product->setCurrency($requestParams['currency']);

            $errors = $this->validator->validate($product);

            if (count($errors) > 0) {
                throw new WrongParamsException((string) $errors, Response::HTTP_NOT_ACCEPTABLE);
            }

            $handledStamp = $this->messageBus
                ->dispatch(new EditProductCommand(
                    $product->getId(),
                    $product->getTitle(),
                    $product->getPrice(),
                    $product->getCurrency()
                ))
                ->last(HandledStamp::class)
                ->getResult();

            $response = $this->parseResponse(
                self::SUCCESSFUL_MESSAGE,
                Response::HTTP_OK,
                $handledStamp->toArray()
            );

        } catch (InvalidArgumentException $exception) {
            $response = $this->parseResponse($exception->getMessage(), Response::HTTP_NOT_ACCEPTABLE);
        } catch (\Exception $exception) {
            $response = $this->parseResponse($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse($response, $response['code']);
    }
}
