<?php
declare(strict_types=1);

namespace App\Service\Query;

use App\Message\Query\Product as ProductQuery;
use App\Service\ServiceResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Uid\Uuid;

class GetProductService extends ServiceResponse
{
    private const SUCCESSFUL_MESSAGE = 'Executed successfully.';

    public function __construct(
        private ValidatorInterface $validator,
        private MessageBusInterface $messageBus,
        private ExchangeRateService $rateService
    ) {}

    public function process(string $productId, ?string $currency): JsonResponse
    {
        try {
            $handledStamp = $this->messageBus
                ->dispatch(new ProductQuery(Uuid::fromString($productId)))
                ->last(HandledStamp::class);

            $product[] = $handledStamp->getResult()->toArray();

            if ($currency) {
                $product = $this->rateService->exchange($product, $currency);
            }

            $response = $this->parseResponse(
                self::SUCCESSFUL_MESSAGE,
                Response::HTTP_OK,
                $product[0]
            );

        } catch (InvalidArgumentException $exception) {
            $response = $this->parseResponse($exception->getMessage(), Response::HTTP_NOT_ACCEPTABLE);
        } catch (\Exception $exception) {
            $response = $this->parseResponse($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse($response, $response['code']);
    }
}