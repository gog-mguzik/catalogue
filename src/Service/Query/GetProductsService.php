<?php
declare(strict_types=1);

namespace App\Service\Query;

use App\Message\Query\Products;
use App\Service\ServiceResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use InvalidArgumentException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class GetProductsService extends ServiceResponse
{
    private const SUCCESSFUL_MESSAGE = 'Executed successfully.';
    private const LIMIT_PER_PAGE = 3;

    public function __construct(
        private ValidatorInterface $validator,
        private MessageBusInterface $messageBus,
        private ExchangeRateService $rateService
    ) {}

    public function process(int $page, ?string $currency): JsonResponse
    {
        try {
            $handledStamp = $this->messageBus
                ->dispatch(new Products($page, self::LIMIT_PER_PAGE))
                ->last(HandledStamp::class);

            $result = $handledStamp->getResult();

            if ($currency) {
                $result['products'] = $this->rateService->exchange($result['products'], $currency);
            }

            $response = $this->parseResponse(
                self::SUCCESSFUL_MESSAGE,
                Response::HTTP_OK,
                $result
            );

        } catch (InvalidArgumentException $exception) {
            $response = $this->parseResponse($exception->getMessage(), Response::HTTP_NOT_ACCEPTABLE);
        } catch (\Exception $exception) {
            $response = $this->parseResponse($exception->getMessage(), $exception->getCode());
        }

        return new JsonResponse($response, $response['code']);
    }
}
