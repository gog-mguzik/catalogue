<?php
declare(strict_types=1);

namespace App\Service\Query;

class ExchangeRateService
{
    private const API_KEY = 'eecc96ae506c342734ae629e';
    private const DEFAULT_CURRENCY = 'USD';

    public function exchange(array $products, string $targetCurrency): array
    {
        $req_url = 'https://v6.exchangerate-api.com/v6/eecc96ae506c342734ae629e/latest/USD';
        $response_json = file_get_contents($req_url);

        if (false !== $response_json) {
            try {
                $response = json_decode($response_json);

                if ('success' === $response->result) {
                    foreach ($products as $productId => $product) {
                        if ($product['currency'] !== $targetCurrency) {
                            $products[$productId]['price'] = round(($product['price'] * $response->conversion_rates->{$targetCurrency}),2);
                            $products[$productId]['defaultCurrency'] = $product['currency'];
                            $products[$productId]['currency'] = $targetCurrency;
                        }
                    }
                }
            }
            catch(\Exception $exception) {
                $product['info'] = sprintf(
                    'Couldn\'t exchange from %s to %s. (Reason: %s: )',
                    $product['currency'],
                    $targetCurrency,
                    $exception->getMessage()
                );
            }
        }

        return $products;
    }
}
