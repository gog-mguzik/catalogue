<?php
declare(strict_types=1);

namespace App\Service;

interface ServiceResponseInterface
{
    public function parseResponse($message, $code, $content = []): array;
}
