<?php
declare(strict_types=1);

namespace App\Helper;

use Symfony\Component\HttpFoundation\Request;

class RequestHelper
{
    public function getRequestParams(Request $request, $associative = true): array
    {
        return json_decode($request->getContent(), $associative);
    }
}
