<?php
declare(strict_types=1);

namespace App\Helper;

class PriceHelper
{
    public function changePriceToInt($price): int
    {
        if (is_int($price)) return $price * 100; else {
            $price = str_replace(",", ".", (string) $price);

            $price = (int) round($price * 100);
        }

        return $price;
    }
}
