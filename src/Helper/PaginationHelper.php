<?php
declare(strict_types=1);

namespace App\Helper;

class PaginationHelper
{
    public function calculatePagination(int $rows, int $page, int $limit): array
    {
        $maxPages = (int) ceil($rows / $limit);

        if ($page > 1) {
            $pagination['prev'] = $page - 1 ;
        }

        $pagination['current'] = $page;
        $pagination['pages'] = $maxPages;

        if ($page < $maxPages) {
            $pagination['next'] = $page + 1;
        }

        return $pagination;
    }
}