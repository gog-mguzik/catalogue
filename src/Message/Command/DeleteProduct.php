<?php
declare(strict_types=1);

namespace App\Message\Command;

use Symfony\Component\Uid\Uuid;

final class DeleteProduct
{
    public function __construct(private Uuid $productId) { }

    public function getProductId(): Uuid
    {
        return $this->productId;
    }
}
