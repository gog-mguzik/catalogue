<?php
declare(strict_types=1);

namespace App\Message\Command;

final class AddProduct
{
    public function __construct(private string $title, private int $price, private string $currency) { }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }
}
