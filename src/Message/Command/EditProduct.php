<?php
declare(strict_types=1);

namespace App\Message\Command;

use Symfony\Component\Uid\Uuid;

final class EditProduct
{
    public function __construct(
        private Uuid $productId,
        private string $title,
        private int $price,
        private string $currency
    ) { }

    public function getProductId(): Uuid
    {
        return $this->productId;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }
}
