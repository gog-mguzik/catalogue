<?php
declare(strict_types=1);

namespace App\Message\Query;

use Symfony\Component\Uid\Uuid;

final class Product
{
    public function __construct(private Uuid $productId) { }

    public function getProductId(): Uuid
    {
        return $this->productId;
    }
}
