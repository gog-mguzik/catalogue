<?php
declare(strict_types=1);

namespace App\MessageHandler\Command;

use App\Entity\Product;
use App\Exception\CanNotAddProductException;
use App\Message\Command\AddProduct;
use App\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class AddProductHandler implements MessageHandlerInterface
{
    public function __construct(private ProductRepository $productRepository) { }

    public function __invoke(AddProduct $newProduct): Product|CanNotAddProductException
    {
        try {
            $this->productRepository->save(
                $product = new Product($newProduct->getTitle(), $newProduct->getPrice(), $newProduct->getCurrency())
            );

            return $product;
        } catch (\Exception $exception) {
            throw new CanNotAddProductException($exception);
        }
    }
}
