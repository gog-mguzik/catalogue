<?php
declare(strict_types=1);

namespace App\MessageHandler\Command;

use App\Entity\Product;
use App\Exception\CanNotEditProductException;
use App\Message\Command\EditProduct;
use App\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class EditProductHandler implements MessageHandlerInterface
{
    public function __construct(private ProductRepository $productRepository) { }

    public function __invoke(EditProduct $editProduct): Product|CanNotEditProductException
    {
        try {
            $product = $this->productRepository->find($editProduct->getProductId());

            $product->setTitle($editProduct->getTitle());
            $product->setPrice($editProduct->getPrice());
            $product->setCurrency($editProduct->getCurrency());
            $product->setUpdated();

            $this->productRepository->save($product);

            return $product;
        } catch (\Exception $exception) {
            throw new CanNotEditProductException($exception);
        }
    }
}
