<?php
declare(strict_types=1);

namespace App\MessageHandler\Command;

use App\Exception\CanNotDeleteProductException;
use App\Message\Command\DeleteProduct;
use App\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class DeleteProductHandler implements MessageHandlerInterface
{
    public function __construct(private ProductRepository $productRepository) { }

    public function __invoke(DeleteProduct $deleteProduct): bool|CanNotDeleteProductException
    {
        try {
            $this->productRepository->delete(
                $this->productRepository->find($deleteProduct->getProductId())
            );

            return true;
        } catch (\Exception $exception) {
            throw new CanNotDeleteProductException($exception);
        }
    }
}
