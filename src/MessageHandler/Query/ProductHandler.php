<?php
declare(strict_types=1);

namespace App\MessageHandler\Query;

use App\Entity\Product;
use App\Exception\ProductNotFoundException;
use App\Message\Query\Product as ProductQuery;
use App\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ProductHandler implements MessageHandlerInterface
{
    public function __construct(private ProductRepository $productRepository) { }

    public function __invoke(ProductQuery $productQuery): Product|ProductNotFoundException
    {
        $product = $this->productRepository->find($productQuery->getProductId());

        if (is_null($product)) throw new ProductNotFoundException((string) $productQuery->getProductId());

        return $product;
    }
}
