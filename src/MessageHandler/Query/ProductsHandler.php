<?php
declare(strict_types=1);

namespace App\MessageHandler\Query;

use App\Helper\PaginationHelper;
use App\Message\Query\Products as ProductsQuery;
use App\Repository\ProductRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class ProductsHandler implements MessageHandlerInterface
{
    public function __construct(
        private ProductRepository $productRepository,
        private PaginationHelper $paginationHelper
    ) { }

    public function __invoke(ProductsQuery $productsQuery): array
    {
        $pagination = $this->paginationHelper->calculatePagination(
            $this->productRepository->count([]),
            $productsQuery->getPage(),
            $productsQuery->getLimit()
        );

        $products = $this->productRepository->findBy(
            [],
            ['added' => 'ASC'],
            $productsQuery->getLimit(),
            ($productsQuery->getLimit() * ($productsQuery->getPage() - 1))
        );

        return [
            'products' => $this->parseResponse($products),
            'pagination' => $pagination
        ];
    }

    private function parseResponse(array $products): array
    {
        $parsedProducts = [];

        foreach ($products as $product) {
            $parsedProducts[] = $product->toArray();
        }

        return $parsedProducts;
    }
}
