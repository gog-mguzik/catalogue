<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\Command\DeleteProductService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('products/{productId}', name: 'products_v1_delete_product', methods: [Request::METHOD_DELETE])]
class DeleteProductController
{
    public function __construct(private DeleteProductService $service) { }

    public function __invoke(string $productId): JsonResponse
    {
        return $this->service->process($productId);
    }
}
