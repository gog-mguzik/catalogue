<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\Command\AddProductService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('products', name: 'products_v1_add_new_product', methods: [Request::METHOD_POST])]
class AddProductController
{
    public function __construct(private AddProductService $service) { }

    public function __invoke(Request $request): JsonResponse
    {
        return $this->service->process($request);
    }
}
