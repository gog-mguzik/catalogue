<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\Query\GetProductService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('products/{productId}', name: 'products_v1_get_product', methods: [Request::METHOD_GET])]
class GetProductController
{
    public function __construct(private GetProductService $service) { }

    public function __invoke(string $productId): JsonResponse
    {
        return $this->service->process($productId, $_GET['currency'] ?? null);
    }
}
