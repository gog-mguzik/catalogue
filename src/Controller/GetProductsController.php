<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\Query\GetProductsService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('products', name: 'products_v1_get_products', methods: [Request::METHOD_GET])]
class GetProductsController
{
    private const DEFAULT_PAGE = 1;

    public function __construct(private GetProductsService $service) { }

    public function __invoke(): JsonResponse
    {
        return $this->service->process(
            (int) ($_GET['page'] ?? self::DEFAULT_PAGE),
            $_GET['currency'] ?? null
        );
    }
}
