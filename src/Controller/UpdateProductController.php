<?php
declare(strict_types=1);

namespace App\Controller;

use App\Service\Command\UpdateProductService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('products/{productId}', name: 'products_v1_update_product', methods: [Request::METHOD_PUT, Request::METHOD_PATCH])]
class UpdateProductController
{
    public function __construct(private UpdateProductService $service) { }

    public function __invoke(string $productId, Request $request): JsonResponse
    {
        return $this->service->process($productId, $request);
    }
}
