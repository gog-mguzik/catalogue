<?php
declare(strict_types=1);

namespace App\Exception;

class WrongParamsException extends \Exception
{
    public function __construct(protected string $errorMessage, protected int $errorCode)
    {
        parent::__construct(
            $errorMessage,
            $errorCode
        );
    }
}