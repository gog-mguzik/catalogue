<?php
declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class CanNotEditProductException extends \Exception
{
    public function __construct(protected $message)
    {
        parent::__construct(
            sprintf('Can\'t edit product: %s because: ', $this->message),
            Response::HTTP_BAD_REQUEST
        );
    }
}