<?php
declare(strict_types=1);

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;

class ProductNotFoundException extends \Exception
{
    public function __construct(protected string $productId)
    {
        parent::__construct(
            sprintf('NOT found product ID: %s.', $this->productId),
            Response::HTTP_NOT_FOUND
        );
    }
}
