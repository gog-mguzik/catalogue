# API Catalogue
API wystawiłem do testów. Można na nie uderzać Postmanem (konfiguracja do  [pobrania](https://www.getpostman.com/collections/2c2eee625d32b3d6559f))

### Routes

- List all of the products
- Add a new product
- Product details
- Update product title | price | currency
- Update product title & price & currency
- Remove a product

### List all of the products

Dodanych jest 5 produktów z treści zadania. Produkty są stronnicowane 3 na stronę. W celu przejścia do kolejnej strony, należy do adresu dodać `?page=2`.

Dodatkowo, można przeliczyć wszystkie produkty według każdej możliwej waluty. Wystarczy dodać `?currency=PLN`. Wyniki zostaną przeliczone z aktualnym kursem danej waluty (domyślna waluta w API to USD). Dodatkowo, w wyniku zostanie zwrócona domyślna waluta produktu (z jakiego została przeliczona).

### Add a new product
Dodanie produktu następuje poprzez wysłanie parametrów `POST`'em' json'em, np.:
```
{
    "name": "Bloodborne",
    "price": 5.99,
    "currency": "USD"
}
```

###  Update product title | price | currency oraz Update product title & price & currency
Uderzenie na wykonać z dowolną ilością parametrów `PUT`'em oraz `PATCH`'em

###  Remove a product
W celu usunięcia produktu należy wykonać uderzenie `DELETE`em